package main

import gl "shared:odin-gl"
import glfw "shared:odin-glfw"
import "core:fmt"
import "core:time"


main :: proc() {
    if !glfw.init() {
        fmt.println("glfw init failed, try again later");
        assert(false);
    }
    window := glfw.init_helper(640,480,"test",3,3,4,false,false,false);
    if window == nil {
        fmt.println("glfw window creation failed, try again later");
    }
    glfw.make_context_current(window);
    gl.load_up_to(3, 3, glfw.set_proc_address);
    glfw.set_framebuffer_size_callback(window, glfw.Framebuffer_Size_Proc(framebuffer_size_proc));
    cur: time.Tick;
    vert_shader_source := `#version 330 core
layout (location = 0) in vec3 aPos;
void main()
{
    gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0f);
}`;
    frag_shader_source := `#version 330 core
out vec4 frag_color;

void main() {
    frag_color = vec4(1.0f, 1.0f, 1.0f, 1.0f);
}`;

    shader_program, success := gl.load_shaders_source(vert_shader_source, frag_shader_source);
    fmt.println(success);

    vert_shader := gl.CreateShader(gl.VERTEX_SHADER);

    vertices := [9]f32{-0.5, -0.5, 0.0,
                        0.5, -0.5, 0.0,
                        0.0,  0.5, 0.0};
    vbo, vao: u32;
    gl.GenVertexArrays(1, &vao);
    gl.BindVertexArray(vao);
    gl.GenBuffers(1, &vbo);
    gl.BindBuffer(gl.ARRAY_BUFFER, vbo);
    gl.BufferData(gl.ARRAY_BUFFER, len(vertices), &vertices, gl.STATIC_DRAW);
    gl.VertexAttribPointer(0, 3, gl.FLOAT, gl.FALSE, 3 * 4, nil);
    gl.EnableVertexAttribArray(0);
    gl.BindBuffer(gl.ARRAY_BUFFER, 0);
    gl.BindVertexArray(0);

    for !glfw.window_should_close(window) {
        cur = time.tick_now();
        process_input(window);
        gl.ClearColor(0.2, 0.3, 0.3, 1.0);
        gl.Clear(gl.COLOR_BUFFER_BIT);

        gl.UseProgram(shader_program);
        gl.BindVertexArray(vao);
        gl.DrawArrays(gl.TRIANGLES, 0, 3);

        glfw.swap_buffers(window);
        glfw.poll_events();
        //fmt.println(1000 / time.duration_milliseconds(time.tick_since(cur)));
    }
    glfw.destroy_window(window);
    glfw.terminate();
}

framebuffer_size_proc :: proc(window: glfw.Window_Handle, width, height: i32) {
    gl.Viewport(0, 0, width, height);
}

process_input :: proc(window: glfw.Window_Handle) {
    if glfw.get_key(window, .KEY_ESCAPE) == .PRESS {
        glfw.set_window_should_close(window, true);
    }

